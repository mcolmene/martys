export const breakfastFood = {
  {
    name: 'Pancakes',
    description: "Delicious Pancakes",
    ingredients: [
      "milk", "eggs", "pancake mix"
    ],
    sides: ["Eggs", "Potatoes", "Bacon"]
    rating: 4,
    imageSrc: "#",
  }
}
export const lunchFood = {
  {
    name: 'Burger',
    description: "This is a burger",
    ingredients: [
      "meat", "tomatoes", "cheese"
    ],
    sides: ["French Fries", "Fruit", "Pickles"]
    rating: 4,
    imageSrc: "#",
  }
}
export const dinnerFood = {
  {
    name: 'Enchiladas',
    description: "Yum Enchiladas",
    ingredients: [
      "cheese", "tomatoe sauce", "tortilla"
    ],
    sides: ["Beans", "Rice"]
    rating: 4,
    imageSrc: "#",
  }
}
