import React from 'react'
import ReactDOM from 'react-dom'
import App from './Routes.js'

ReactDOM.render(<App />, document.getElementById('root'))
